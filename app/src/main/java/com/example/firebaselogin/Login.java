package com.example.firebaselogin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Login extends AppCompatActivity {
    private EditText Et_email,Et_password;
    private Button btn_login;
    private ProgressBar progressBar;
    private TextView tv_link_regiester,tv_forget_pswed;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Et_email=findViewById(R.id.Et_mail);
        Et_password=findViewById(R.id.Et_Pswd);
        btn_login=findViewById(R.id.btnLogin);
        tv_link_regiester=findViewById(R.id.link_to_register);
        progressBar=findViewById(R.id.progressberId);
        tv_forget_pswed=findViewById(R.id.forget_pswd);


         firebaseAuth=FirebaseAuth.getInstance();





        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email=Et_email.getText().toString().trim();
                String password=Et_password.getText().toString().trim();


                if (TextUtils.isEmpty(email)){
                    Et_email.setError("Email is Required");
                    return;
                }
                if (TextUtils.isEmpty(password)){
                    Et_email.setError("Email is Required");
                    return;
                }
                if (password.length()<6){
                    Et_password.setError("Password Must be 6 Charactors");
                    return;
                }progressBar.setVisibility(View.VISIBLE);

                firebaseAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()){
                            Toast.makeText(getApplicationContext(),"Login Sucessfull",Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(),MainActivity.class));
                        }
                        else{
                            Toast.makeText(getApplicationContext(),"!Error"+task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.GONE);
                        }

                    }
                });


            }
        });

        tv_link_regiester.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),Regiester.class));

            }
        });
        tv_forget_pswed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText resrtmail=new EditText(v.getContext());

                AlertDialog.Builder pswddialogbuilder=new AlertDialog.Builder(v.getContext());
                 pswddialogbuilder.setTitle("Reset Password ?");
                 pswddialogbuilder.setMessage("Enter your mail To Recived Reset Link.");
                 pswddialogbuilder.setView(resrtmail);

                 pswddialogbuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                     @Override
                     public void onClick(DialogInterface dialog, int which) {
                         String email=resrtmail.getText().toString();

     firebaseAuth.sendPasswordResetEmail(email).addOnSuccessListener(new OnSuccessListener<Void>() {
         @Override
         public void onSuccess(Void aVoid) {
             Toast.makeText(getApplicationContext(),"Reset Link to send To Email",Toast.LENGTH_SHORT).show();

         }
     }).addOnFailureListener(new OnFailureListener() {
         @Override
         public void onFailure(@NonNull Exception e) {
             Toast.makeText(getApplicationContext(),"Error ! Reset Link is not sent "+e.getMessage(),Toast.LENGTH_SHORT).show();

         }
     });
                     }
                 });

                pswddialogbuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

   pswddialogbuilder.create().show();

            }
        });



    }
}
