package com.example.firebaselogin;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import javax.annotation.Nullable;

public class MainActivity extends AppCompatActivity {
    private
    android.widget.Button btn_logout;
    private ImageView imageView;
    private TextView tv_fullname,tv_email,tv_phone;
    FirebaseAuth firebaseAuth;
    FirebaseFirestore firestore;
    String userId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_logout=findViewById ( com.example.firebaselogin.R.id.loginOut );
        imageView=findViewById(R.id.imageView);

        tv_fullname=findViewById(R.id.pro_fullname);
        tv_email=findViewById(R.id.pro_email);
        tv_phone=findViewById(R.id.pro_phonenumber);
        firebaseAuth=FirebaseAuth.getInstance();
        firestore=FirebaseFirestore.getInstance();

        userId=firebaseAuth.getCurrentUser().getUid();

        DocumentReference documentReference=firestore.collection("UserId").document(  userId);
        documentReference.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                tv_fullname.setText(documentSnapshot.getString("fname"));
                tv_email.setText(documentSnapshot.getString("email"));
                tv_phone.setText(documentSnapshot.getString("pnumber"));
            }
        });

        btn_logout.setOnClickListener ( new android.view.View.OnClickListener ( ) {
            @Override
            public
            void onClick ( android.view.View v ) {
            com.google.firebase.auth.FirebaseAuth.getInstance ( ).signOut ();
                startActivity ( new android.content.Intent ( getApplicationContext (),Login.class ) );
                finish ();
            }
        } );


    }




}
