package com.example.firebaselogin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class Regiester extends AppCompatActivity {
    private EditText Et_fullname,Et_Email,Et_password,Et_phone;
    private Button btn_regiester;
    private TextView Tv_crateReg_login;
    private ProgressBar progressBar;
    FirebaseAuth firebaseAuth;
    FirebaseFirestore firestore;
    String userId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regiester);

        Et_fullname=findViewById(R.id.reg_fullname);
        Et_Email=findViewById(R.id.reg_email);
        Et_password=findViewById(R.id.reg_password);
        btn_regiester=findViewById(R.id.btnRegister);
        Tv_crateReg_login=findViewById(R.id.link_to_login);
        progressBar=findViewById(R.id.progressberId);
        Et_phone=findViewById(R.id.reg_phoneNumber);

        firebaseAuth = FirebaseAuth.getInstance ();
        firestore=FirebaseFirestore.getInstance();

        if (firebaseAuth.getCurrentUser()!=null){
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
            finish();
        }


 btn_regiester.setOnClickListener(new View.OnClickListener() {
     @Override
     public void onClick(View v) {

       final String email=  Et_Email.getText().toString().trim();
       String pswd=  Et_password.getText().toString().trim();

       final String fullname=Et_fullname.getText().toString().trim();
       final String phone_number=Et_phone.getText().toString().trim();


       if (TextUtils.isEmpty(email)){
               Et_Email.setError("Email is Required");
               return;
           }
           if (TextUtils.isEmpty(pswd)){
               Et_password.setError("Password is Required");
               return;
       }
       if (pswd.length() <6){
           Et_password.setError("Password Must be 6 Charactors");
           return;
       }
       progressBar.setVisibility(View.VISIBLE);
       firebaseAuth.createUserWithEmailAndPassword(email,pswd).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
           @Override
           public void onComplete(@NonNull Task<AuthResult> task) {
               if (task.isSuccessful()){
                   Toast.makeText(getApplicationContext(),"User Created.",Toast.LENGTH_SHORT).show();
                   userId=firebaseAuth.getCurrentUser().getUid();
                   DocumentReference documentReference=firestore.collection("user").document(userId);
                   Map<String,Object> user=new HashMap<>();
                   user.put("fname",fullname);
                   user.put("pnumber",phone_number);
                   user.put("email",email);
                   documentReference.set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                       @Override
                       public void onSuccess(Void aVoid) {
                           Toast.makeText(getApplicationContext(),"onSuccess: User profile is created for "+userId,Toast.LENGTH_SHORT).show();

                       }
                   });

                   startActivity(new Intent(getApplicationContext(),MainActivity.class));
               }else{
                   Toast.makeText(getApplicationContext(),"Error! :"+task.getException().getMessage(),Toast.LENGTH_SHORT).show();
                   progressBar.setVisibility(View.GONE);
               }

           }
       });

     }
 });



  Tv_crateReg_login.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
          startActivity(new Intent(getApplicationContext(),Login.class));

      }
  });



    }
}
